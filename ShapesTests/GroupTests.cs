﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapesLibrary;

namespace ShapesTests
{
    [TestClass]
    public class GroupTests
    {

        [TestMethod]
        public void GetExtent_Returns_aCorrect_Value_of_Area()
        {
            Shape sphere = new Sphere("Test sphere", 2.4),
                  circle = new Circle("Test circle", 2.1),
                  rectangle = new Rectangle("Test rectangle", 4, 9),
                  group = new Group("Test group");

            group.Add(sphere);
            group.Add(circle);
            group.Add(rectangle);

            double expectedValue = sphere.GetExtent() + circle.GetExtent() +
                                   rectangle.GetExtent();

            Assert.AreEqual(expectedValue, group.GetExtent());
        }

        [TestMethod]
        public void Group_Add_aShape()
        {
            Group shape = new Group("Test group");

            shape.Add(new Sphere("Test sphere", 2.5));

            Assert.AreEqual(1, shape.Count);
        }

        [TestMethod]
        public void Group_Rmove_aShape()
        {
            Group shape = new Group("Test group");

            Sphere sphere = new Sphere("Test sphere", 2.5);

            shape.Add(sphere);
            shape.Remove(sphere);

            Assert.AreEqual(0, shape.Count);
        }
    }
}
