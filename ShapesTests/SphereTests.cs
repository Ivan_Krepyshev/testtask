﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapesLibrary;
using System;

namespace ShapesTests
{
    [TestClass]
    public class SphereTests
    {
        [TestMethod]
        public void GetExtent_Returns_aCorrect_Value_of_Area()
        {
            double radius = 2.5;

            Sphere sphere = new Sphere("Test sphere", radius);

            double expectedValue = 4 * Math.PI * Math.Pow(radius, 2);

            Assert.AreEqual(expectedValue, sphere.GetExtent());
        }
    }
}
