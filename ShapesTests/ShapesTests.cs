﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapesLibrary;

namespace ShapesTests
{
    [TestClass]
    public class ShapesTests
    {

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void Throws_NotImplementedException_When_aLeaf_Tries_To_Add_aShape()
        {
            Shape shape = new Circle("Test circle", 2);
            shape.Add(new Rectangle("Test rectangle", 2, 4));
        }

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void Throws_NotImplementedException_When_aLeaf_Tries_To_Remove_aShape()
        {
            Shape shape = new Circle("Test circle", 2);
            shape.Remove(new Rectangle("Test rectangle", 2, 4));
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void Groups_Add_Method_Throws_NullReferenceException_When_Input_Is_Null()
        {
            Shape shape = new Group("Test group");
            shape.Add(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void Groups_Remove_Method_Throws_NullReferenceException_When_Input_Is_Null()
        {
            Shape shape = new Group("Test group");
            shape.Remove(null);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void Groups_Constructor_Throws_NullReferenceException_When_Shapes_Are_Null()
        {
            Shape shape = new Group("Test group", null);
        }

        [TestMethod]
        public void Leaf_Calls_GetExtent()
        {
            Shape shape = new Sphere("Test spere", 3);

            Assert.IsTrue(shape.GetExtent() >= 0);
        }

        [TestMethod]
        public void Group_Calls_GetExtent()
        {
            double radius = 2.5;

            Shape shape = new Group("Test group");

            shape.Add(new Sphere("Test shere", radius));
            shape.Add(new Circle("Test shere", radius));

            double expectedResult = Math.PI * Math.Pow(radius, 2) +
                                    4 * Math.PI * Math.Pow(radius, 2);

            Assert.AreEqual(expectedResult, shape.GetExtent());
        }

    }
}
