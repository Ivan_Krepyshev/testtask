﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapesLibrary;

namespace ShapesTests
{
    [TestClass]
    public class RectangleTests
    {
        [TestMethod]
        public void GetExtent_Returns_aCorrect_Value_of_Area()
        {
            double width = 2.5,
                   height = 4;

            Rectangle rectangle = new Rectangle("Test rectangle", height, width);

            double expectedValue = width * height;

            Assert.AreEqual(expectedValue, rectangle.GetExtent());
        }
    }
}
