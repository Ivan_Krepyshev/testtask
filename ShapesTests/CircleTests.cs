﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShapesLibrary;
using System;

namespace ShapesTests
{
    [TestClass]
    public class CircleTests
    {
        [TestMethod]
        public void GetExtent_Returns_aCorrect_Value_of_Area()
        {
            double radius = 2.5;

            Circle circle = new Circle("Test circle", radius);

            double expectedValue = Math.PI * Math.Pow(radius, 2);

            Assert.AreEqual(expectedValue, circle.GetExtent());
        }
    }

}
