﻿using System;
using System.Collections.Generic;


namespace ShapesLibrary
{
    /// <summary>
    /// This class implements a shape as a compound shape.
    /// It stores other shapes in a List
    /// </summary>
    public class Group : Shape
    {
        /// <summary>
        /// Gets the number of shapes contained in the Group.
        /// </summary>
        public int Count
        {
            get
            {
                return shapes.Count;
            }
        }
        /// <summary>
        /// List of the shapes
        /// </summary>
        protected List<Shape> shapes;
        /// <summary>
        /// Gets or sets the shape at the specified index.
        /// </summary>
        /// <param name="index">Index of the shape</param>
        /// <returns>The shape</returns>
        public Shape this[int index]
        {
            get
            {
                return shapes[index];
            }
            set
            {
                shapes[index] = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the Group class that has 
        /// the specified initial name and default initial instance of the list of shapes.
        /// </summary>
        /// <param name="name">The name</param>
        public Group(string name) : base(name)
        {
            shapes = new List<Shape>();
        }
        /// <summary>
        /// Initializes a new instance of the Group class that has 
        /// the specified initial name and specified instance of the list of shapes.
        /// </summary>
        /// <remarks>
        /// Throws NullReferenceException when the specified list is null.
        /// </remarks>
        /// <param name="name">The name</param>
        /// <param name="shapes">The list of shapes</param>
        public Group(string name, List<Shape> shapes) : base(name)
        {
            if (shapes == null)
                throw new NullReferenceException("Shapes can not be null");

            this.shapes = shapes;
        }

        /// <summary>
        /// Returns the area of the shape.
        /// </summary>
        /// <returns>The area</returns>
        public override double GetExtent()
        {
            if(shapes.Count > 0)
            {
                foreach(var shape in shapes)
                {
                    area += shape.GetExtent();
                }
            }

            return area;
        }
        /// <summary>
        /// Adds a shape to the end of the group.
        /// </summary>
        /// <remarks>
        /// If the specified shape is null, it throws new NullReferenceException
        /// </remarks>
        /// <param name="shape">The shape</param>
        public override void Add(Shape shape)
        {
            if (shape == null)
                throw new NullReferenceException("Shape can not be null");

            shapes.Add(shape);
        }
        /// <summary>
        /// Removes the first occurrence of a specific shape from the Group.
        /// </summary>
        /// <remarks>
        /// If the specified shape is null, it throws new NullReferenceException
        /// </remarks>
        /// <param name="shape">The shape</param>
        public override void Remove(Shape shape)
        {
            if (shape == null)
                throw new NullReferenceException("Shape can not be null");

            shapes.Remove(shape);
        }
    }
}
