﻿using System;


namespace ShapesLibrary
{

    /// <summary>
    /// This class implements a shape as a sphere.
    /// </summary>
    public class Sphere : Shape
    {
        /// <summary>
        /// Radius of the sphere
        /// </summary>
        protected double radius;

        /// <summary>
        /// Initializes a new instance of the Sphere class that has
        /// the specified initial name and radius.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="radius">The radius of the sphere</param>
        public Sphere(string name, double radius) : base(name)
        {
            this.radius = radius;

            area = 4 * Math.PI * Math.Pow(radius, 2);
        }

        /// <summary>
        /// Returns the area of the shape.
        /// </summary>
        /// <returns>The area</returns>
        public override double GetExtent()
        {
            return area;
        }

        /// <summary>
        /// This class doesn't support this method
        /// </summary>
        /// <remarks>
        /// The default implementation must throw NotImplementedException.
        /// </remarks>
        /// <param name="shape"></param>
        public override void Add(Shape shape)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// This class doesn't support this method
        /// </summary>
        /// <remarks>
        /// The default implementation must throw NotImplementedException.
        /// </remarks>
        /// <param name="shape"></param>
        public override void Remove(Shape shape)
        {
            throw new NotImplementedException();
        }

    }
}
