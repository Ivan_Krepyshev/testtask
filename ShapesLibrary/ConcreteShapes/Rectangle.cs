﻿using System;


namespace ShapesLibrary
{

    /// <summary>
    /// This class implements a shape as a rectangle.
    /// </summary>
    public class Rectangle : Shape
    {
        /// <summary>
        /// Height of the rectangle
        /// </summary>
        protected double height;
        /// <summary>
        /// Width of the rectangle
        /// </summary>
        protected double width;

        /// <summary>
        /// Initializes a new instance of the Rectangle class that has
        /// the specified initial name, height and width.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="height">The height</param>
        /// <param name="width">The width</param>
        public Rectangle(string name, double height, double width) 
            : base(name)
        {
            this.height = height;
            this.width = width;

            area = height * width;
        }

        /// <summary>
        /// Returns the area of the shape.
        /// </summary>
        /// <returns>The area</returns>
        public override double GetExtent()
        {
            return area;
        }

        /// <summary>
        /// This class doesn't support this method
        /// </summary>
        /// <remarks>
        /// The default implementation must throw NotImplementedException.
        /// </remarks>
        /// <param name="shape"></param>
        public override void Add(Shape shape)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// This class doesn't support this method
        /// </summary>
        /// <remarks>
        /// The default implementation must throw NotImplementedException.
        /// </remarks>
        /// <param name="shape"></param>
        public override void Remove(Shape shape)
        {
            throw new NotImplementedException();
        }

    }
}
