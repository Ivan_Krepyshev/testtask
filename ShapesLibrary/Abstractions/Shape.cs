﻿namespace ShapesLibrary
{

    /// <summary>
    /// Shape provides the base interface and
    /// implementation for all shapes
    /// </summary>
    public abstract class Shape
    {
        /// <summary>
        /// Area of the shape
        /// </summary>
        protected double area;
        /// <summary>
        /// Name of the shape
        /// </summary>
        protected string name;

        /// <summary>
        /// Provides the base implementation for
        /// the shape class
        /// </summary>
        /// <param name="name">The name of the shape</param>
        public Shape(string name)
        {
            this.name = name;
        }

        /// <summary>
        /// Adds a shape to the group of shapes.
        /// </summary>
        /// <remarks>
        /// This method must have the implementation only in groups
        /// of shapes. In a single shape method must throw NotImplementedException.
        /// </remarks>
        /// <param name="shape">The shape</param>
        public abstract void Add(Shape shape);
        /// <summary>
        /// Removes the first occurrence of a specific shape from the group
        /// of shapes.
        /// </summary>
        /// <remarks>
        /// This method must have the implementation only in groups
        /// of shapes. In a single shape method must throw NotImplementedException.
        /// </remarks>
        /// <param name="shape">The shape</param>
        public abstract void Remove(Shape shape);
        /// <summary>
        /// Returns the area of the shape.
        /// </summary>
        /// <returns>The area</returns>
        public abstract double GetExtent();

        /// <summary>
        /// Returns a string that represents the current shape.
        /// </summary>
        /// <returns>String representation of the current shape</returns>
        public override string ToString()
        {
            return this.name;
        }
    }
}
